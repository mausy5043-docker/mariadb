#!/bin/bash

# Find out where we're running from
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -f Dockerfile ]; then
  echo "Not allowed. First execute:"
  echo "cd ${SCRIPT_DIR}"
  echo " Then try again."
  exit 1
fi

source "${SCRIPT_DIR}"/config.txt

# build a local image
docker build --rm -t "${image}" .
