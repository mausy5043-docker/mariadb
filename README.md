# docker-mariadb

It is advised to clone the [Git repository](https://gitlab.com/mausy5043-docker/mariadb) and build your own image from that.

## Installing
The preferred procedure is:
```
git clone https://gitlab.com/mausy5043-docker/mariadb
cd mariadb
```
Review the default settings in `config\*.conf`.
Create the file `${HOME}/.config/docker/mariadb/mariadb.conf`. This holds two variables:
`PSK=root_password` ;
This will set the root password for `mysql`.
`VSOURCE=/srv/array1/datastore/mysql` ;
This is the path that will be attached to `/var/lib/mysql` inside the container. It allows for storing the databases on the host non-volatily.

```
./build.sh local
./run.sh local
```


## Build a fresh image

```
./build.sh local
```
This builds the image with tag `local`


## Run the image

```
./run.sh local
```
This runs a container using the image tagged `local`
It uses a custom configuration that is kept in `${HOME}/.config/docker/mariadb/mariadb.conf`.
It also attaches root's home directory `/root` to `${HOME}/.config/docker/mariadb/root/`. This allows you to store a `.my.cnf` on the host. It is advised to also create a `.bashrc` and a `.profile` file.


## Stop a running container

```
./stop.sh
```
Stops the container and then deletes it. This allows for immediately running a container with the same name without the need to `docker rm` it manually.


## Updating
FIRST!! Make a backup copy of  `config\*.conf` for your own mental health.

```
./update.sh [--all]
```
This force-pulls the files from Git. Use the `--all` switch to rebuild the image and restart the container immediately.
Be aware that this will overwrite any changes you may have made to the default configuration stored in `config\*.conf`!

DISCLAIMER:
Use this software at your own risk! We take no responsibility for ANY data loss.
We guarantee no fitness for any use specific or otherwise.
