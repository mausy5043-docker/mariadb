#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -f Dockerfile ]; then
  echo "Not allowed. First execute:"
  echo "cd ${script_dir}"
  echo " Then try again."
  exit 1
fi

# read MGR, USR and PSK parameters from local .config file
reposettings="${script_dir}/mariadb.conf"
usersettings="${HOME}/.config/docker/mariadb.conf"
# shellcheck disable=SC1090
source "${reposettings}"
if [ -f "${usersettings}" ]; then
  # shellcheck disable=SC1090
  source "${usersettings}"
fi

source "${script_dir}"/config.txt

docker run -d \
           -e MYSQL_ROOT_PASSWORD="${PSK}" \
           -h "${container}" \
           -p 3306:3306 \
           -v "${PWD}/config/etc_syslog-ng_conf.d_remote.conf":/etc/syslog-ng/conf.d/remote.conf \
           -v "${HOME}/.config/docker/"${container}"/root/":/root/ \
           -v "${VSOURCE}":/var/lib/mysql \
           --name "${container}" \
           --restart unless-stopped \
           "${image}"

# Always run with the latest updates.
docker exec -it "${container}" apt-get update
docker exec -it "${container}" apt-get -y upgrade

docker logs "${container}"
# enable for testing purposes:
docker exec -it "${container}" /bin/bash
