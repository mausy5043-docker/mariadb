#!/bin/bash

# Find out where we're running from
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -f Dockerfile ]; then
  echo "Not allowed. First execute:"
  echo "cd ${SCRIPT_DIR}"
  echo " Then try again."
  exit 1
fi

cmd="${1}"

git fetch origin master
git reset --hard FETCH_HEAD
git clean -df
git pull

if [ "${cmd}" == "--all" ]; then
  "${SCRIPT_DIR}"/build.sh || exit 1
  "${SCRIPT_DIR}"/stop.sh
  "${SCRIPT_DIR}"/run.sh || exit 1
fi
